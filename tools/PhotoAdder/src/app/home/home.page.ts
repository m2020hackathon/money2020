import { Component } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {HttpClient} from "@angular/common/http";
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  headers: HttpHeaders;
  
    constructor(private camera: Camera, private client: HttpClient) 
    { 
      this.headers = new HttpHeaders();
      this.headers.append('Content-Type', 'application/json');
    }

  openCamera(event) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA
    }

    this.camera.getPicture(options).then((imageData) => {
      //let base64Image = "data:image/jpeg;base64," + imageData;
      //this.base64.encodeFile(imageData).then((base64File: string) => {
      //  console.log(base64File);
        this.client.post("http://10.42.0.105:8188/api/photo",
        {
            "imageData": imageData
        },{headers: this.headers}).subscribe(result => { },
        error => alert(JSON.stringify(error)));
        }, (err) => {
        console.log(err);
      });

    }, (err) => {
      // Handle error
     });
  }
}


