﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2020.Web.Data.Repositories
{
    public class VisaRepository
    {
        public void IdentityDocumentAuthentication()
        {
            var client = new System.Net.Http.HttpClient();

            var content = new System.Net.Http.MultipartFormDataContent("---Iftah_Boundary---");
            byte[] image = new byte[256];
            content.Add(new System.Net.Http.StreamContent(new System.IO.MemoryStream(image)), "Refugee ID", "refugeeid.jpg");
            var response = client.PostAsync("https://sandbox.api.visa.com/identitydociments/v1/documentauthentication", content).Result;
        }
    }
}
