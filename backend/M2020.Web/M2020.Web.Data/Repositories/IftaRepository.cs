﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using M2020.Web.Data.Models;
using Dapper;
using M2020.Web.Api.Models;

namespace M2020.Web.Data.Repositories
{
    public class IftaRepository
    {
        private SqlConnection _sqlConnection = new SqlConnection();

        public IftaRepository()
        {
            _sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Iftaconnection"].ConnectionString;
        }
        public Person GetPersonByAzurePersonId(string azurePersonId)
        {
            return _sqlConnection.Query<Person>(@"SELECT personid, firstname, lastname, persontype, profilepicture, azurepersonid
                    from dbo.person where azurepersonid=@id", new { id = azurePersonId }).FirstOrDefault();
        }
        public Person GetPerson(int userId)
        {
            return _sqlConnection.Query<Person>(@"SELECT personid, firstname, lastname, persontype, profilepicture, azurepersonid
                    from dbo.person where personid=@id", new { id = userId }).FirstOrDefault();
        }
        public bool PostTransaction(Transaction transaction)
        {
            var result = _sqlConnection.Execute("insert into dbo.[transaction](frompersonid, topersonid, amount, datetime) values(@from, @to, @amount, getdate())",
                new { from = transaction.FromPersonId, to = transaction.ToPersonId, amount = transaction.Amount});
            return result == 1;
        }
        public Transaction GetTransaction(int transactionId)
        {
            return _sqlConnection.Query<Transaction>(@"SELECT TransactionId, fromPersonId, ToPersonId, Amount, DateTime from dbo.[transaction] where transactionid=@id",
                new { id = transactionId }).FirstOrDefault();
        }
        public IEnumerable<Transaction> GetTransactionByUser(int userId)
        {
            return _sqlConnection.Query<Transaction>(@"SELECT TransactionId, fromPersonId, ToPersonId, Amount, DateTime 
                from dbo.[transaction] where fromPersonId=@id or toPersonId=@id",
                new { id = userId });
        }
    }
}
