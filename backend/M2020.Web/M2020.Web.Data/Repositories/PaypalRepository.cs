﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace M2020.Web.Data.Repositories
{
    public class PaypalRepository
    {
        public List<Models.Paypal.Link> SendPeerToPeer(Models.Paypal.PeerToPeerRequest request)
        {
            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", "A21AAEd4uMPiYCMli-cnkErNZ4mbFk6cmYQUYXNqOaXvXBUz42BoSLtQvM2jbjlbYQL2PQbw61VOIP-rrjGMCMeUVjRbATVjg");
            var uri = new Uri("https://api.sandbox.paypal.com/v1/payments/personal-payment-tokens");
            var content = new System.Net.Http.StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var postResult = client.PostAsync(uri, content).Result;
            var resultContent = postResult.Content.ReadAsStringAsync().Result;
            var peerResponse = JsonConvert.DeserializeObject<Models.Paypal.PeerToPeerResponse>(resultContent);
            if (postResult.StatusCode != System.Net.HttpStatusCode.OK) return null;
            return peerResponse.status == "CREATED" ? peerResponse.links : null;
        }
    }
}
