﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2020.Web.Data.Models.Cognitive
{
    public class IdentifyResult
    {
        public string FaceId { get; set; }
        public List<IdentifyCandidate> Candidates { get; set; }
        
        //    //[{
        //"faceId": "7f4b2206-8339-491f-876a-934391334e2e",
        //"candidates": [{
        // "personId": "7376c609-557f-4b29-b513-141857e1a3a3",
        //"confidence": 0.58525
        //}]
        //}]

    }
}
