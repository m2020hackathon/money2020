﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2020.Web.Data.Models.Cognitive
{
    public class IdentifyCandidate
    {
        public string PersonId { get; set; }
        public double Confidence { get; set; }
    }
}
