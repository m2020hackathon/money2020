﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2020.Web.Data.Models.Cognitive
{
    public class IdentifyRequest
    {
        //            {
        //                "personGroupId": "hackathon",
        //    "faceIds": [
        //        "7f4b2206-8339-491f-876a-934391334e2e"
        //    ],
        //    "maxNumOfCandidatesReturned": 1,
        //    "confidenceThreshold": 0.5
        //}

        public string PersonGroupId { get; set; }
        public List<string> FaceIds { get; set; }
        public int MaxNumOfCandidatesReturned { get; set; }
        public double ConfidenceThreshold { get; set; }
    }
}
