﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2020.Web.Api.Models
{
    public class Person
    {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public int PersonType { get; set; }
        public string AzurePersonId { get; set; }
    }
}