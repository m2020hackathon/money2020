﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2020.Web.Data.Models.Paypal
{
    public class PeerToPeerRequest
    {
        public Amount amount { get; set; }
        public Payee payee { get; set; }
        public string payment_type { get; set; }
    }
}
