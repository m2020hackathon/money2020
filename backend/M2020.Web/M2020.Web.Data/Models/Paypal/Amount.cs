﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2020.Web.Data.Models.Paypal
{
    public class Amount
    {
        public double value { get; set; }
        public string currency { get; set; }
    }
}
