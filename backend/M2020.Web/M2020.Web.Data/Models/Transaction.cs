﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2020.Web.Data.Models
{
    public class Transaction
    { 
        public int FromPersonId { get; set; }
        public int ToPersonId { get; set; }
        public double Amount { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
    }
}