﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2020.Web.Api.Models
{
    public class Settings
    {
        public readonly string _key = "7134b091e1bd4f1f81af0f79c8012932";
        public readonly string _url = "https://westus.api.cognitive.microsoft.com/face/v1.0/";

        public string Key
        {
            get
            {
                return _key;
            }
        }

        public string Url
        {
            get
            {
                return _url;
            }
        }
    }
}