﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2020.Web.Api.Models
{
    public class Offer
    {
        public int OfferId { get; set; }
        public string OfferDetail { get; set; }
    }
}