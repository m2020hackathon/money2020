﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using M2020.Web.Data.Models;

namespace M2020.Web.Api.Models
{
    public class Consumer : Person
    {
        public List<Transaction> Transactions { get; set; }

    }
}