﻿using M2020.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using M2020.Web.Data.Models;

namespace M2020.Web.Api.Controllers
{
    [RoutePrefix("api/transaction")]
    public class TransactionController : ApiController
    {
        [HttpPost]
        [Route("")]
        public bool AddTransaction(Transaction transaction)
        {
            var repo = new Data.Repositories.IftaRepository();
            return repo.PostTransaction(transaction);
        }
        [HttpPost]
        [Route("{transactionId}")]
        public Transaction GetTransaction(int transactionId)
        {
            var repo = new Data.Repositories.IftaRepository();
            return repo.GetTransaction(transactionId);
        }

        [HttpGet]
        [Route("user/{userId}")]
        public IEnumerable<Transaction> GetTransactionByUser(int userId)
        {
            var repo = new Data.Repositories.IftaRepository();
            return repo.GetTransactionByUser(userId);
        }
    }
}
