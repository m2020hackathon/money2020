﻿using M2020.Web.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace M2020.Web.Api.Controllers
{
    [RoutePrefix("api/photo")]
    public class PhotoController : ApiController
    {
        [HttpPost]
        [Route("")]
        public int AddPhoto(HttpRequestMessage requestObject)
        {
            var objJson = requestObject.Content.ReadAsStringAsync().Result;
            var photoUploadObj = JsonConvert.DeserializeObject<PhotoUpload>(objJson);
            return 1;
        }

        [HttpGet]
        [Route("identify")]
        public Consumer IdentifyConsumer(HttpRequestMessage requestObject)
        {
            return null;
        }
    }
}
