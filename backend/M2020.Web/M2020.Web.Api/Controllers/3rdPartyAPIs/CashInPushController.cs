﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Http;
using System.Security.Cryptography.X509Certificates;

namespace M2020.Web.Api.Controllers._3rdPartyAPIs
{
    public class CashInPushController : ApiController
    {
        // GET: api/CashInPush
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/CashInPush/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/CashInPush
        public void Post([FromBody]string webRequestBody)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
            HttpWebRequest request = WebRequest.Create("https://sandbox.api.visa.com/visadirect/mvisa/v1/cashinpushpayments") as HttpWebRequest;
            // Add headers
            string userID = "6WSVVG4IRBPDP947BZJU21NpJOlfDZ_3pOLfGCN91_lwhHlrg";
            string password = "Z16icVADonsnmVsUX4N";
            string authString = userID + ":" + password;
            var authStringBytes = System.Text.Encoding.UTF8.GetBytes (authString);
            string authHeaderString = Convert.ToBase64String (authStringBytes);
            request.Headers ["Authorization"] = "Basic " + authHeaderString;
            // truning on tracing

            // Add certificate
            var certificate = new X509Certificate2("c:\\sources\\ifta.p12", "changeit");
            request.ClientCertificates.Add(certificate);

            //the web response

            //
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        }

        // PUT: api/CashInPush/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/CashInPush/5
        public void Delete(int id)
        {
        }
    }
}
