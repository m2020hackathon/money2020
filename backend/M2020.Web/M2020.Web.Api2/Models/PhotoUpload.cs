﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2020.Web.Api.Models
{
    public class PhotoUpload
    {
        public int UserId { get; set; }
        public string ImageData { get; set; }
    }
}