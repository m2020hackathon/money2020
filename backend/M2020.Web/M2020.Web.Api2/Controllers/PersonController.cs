﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace M2020.Web.Api2.Controllers
{
    [RoutePrefix("api/person")]
    public class PersonController : ApiController
    {
        [HttpPost]
        [Route("iddocument")]
        public int DocumentCheck(Api.Models.PhotoUpload uploadObj)
        {
            var visaRepo = new Data.Repositories.VisaRepository();
            visaRepo.IdentityDocumentAuthentication();
            return 1;
        }
        [HttpPost]
        [Route("sendmoney")]
        public List<Data.Models.Paypal.Link> SendMoney(Data.Models.Paypal.PeerToPeerRequest request)
        {
            var paypalRepo= new Data.Repositories.PaypalRepository();
            return paypalRepo.SendPeerToPeer(request);
            
        }
    }
}
