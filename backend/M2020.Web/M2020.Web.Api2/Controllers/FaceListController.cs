﻿using M2020.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace M2020.Web.Api.Controllers
{
    [RoutePrefix("api/cognitive/facelist")]
    public class FaceListController : ApiController
    {
        
        [HttpGet]
        [Route("")]
        public string GetFaceLists()
        {
            var client = new HttpClient();

            var settings = new Settings();
            var uri = new Uri(settings.Url+ "facelists");
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "7134b091e1bd4f1f81af0f79c8012932");
            var result = client.GetAsync(uri).Result;
            return result.Content.ReadAsStringAsync().Result;
        }
        [HttpGet]
        [Route("{facelistid}")]
        public string GetFaceListItems(string facelistid)
        {
            var client = new HttpClient();

            var settings = new Settings();
            var uri = new Uri(settings.Url + "facelists/"+facelistid);
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "7134b091e1bd4f1f81af0f79c8012932");
            var result = client.GetAsync(uri).Result;
            return result.Content.ReadAsStringAsync().Result;
        }
    }
}
