﻿using M2020.Web.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace M2020.Web.Api.Controllers
{
    [RoutePrefix("api/photo")]
    public class PhotoController : ApiController
    {
        [HttpPost]
        [Route("")]
        public int AddPhoto(HttpRequestMessage requestObject)
        {
            var objJson = requestObject.Content.ReadAsStringAsync().Result;
            var photoUploadObj = JsonConvert.DeserializeObject<PhotoUpload>(objJson);
            var cognitiveRepo = new Data.Repositories.CognitiveRepository();
            var repo = new Data.Repositories.IftaRepository();
            var person = repo.GetPerson(photoUploadObj.UserId);
            if (person == null) return -1;
            cognitiveRepo.AddFace(person.AzurePersonId, photoUploadObj.ImageData);
            cognitiveRepo.Train();
            return 1;
        }

        [HttpPost]
        [Route("identify")]
        public Person IdentifyConsumer(HttpRequestMessage requestObject)
        {
            var cognitiveRepo = new Data.Repositories.CognitiveRepository();
            var objJson = requestObject.Content.ReadAsStringAsync().Result;
            var photoIdentifyObj = JsonConvert.DeserializeObject<PhotoUpload>(objJson);
            var faceId = cognitiveRepo.Detect(photoIdentifyObj.ImageData);
            if (!string.IsNullOrEmpty(faceId))
            {
                var personId = cognitiveRepo.Identify(faceId);
                if (string.IsNullOrEmpty(personId)) return null;
                var repo = new Data.Repositories.IftaRepository();
                return repo.GetPersonByAzurePersonId(personId);
            }
            return null;
        }
    }
}
