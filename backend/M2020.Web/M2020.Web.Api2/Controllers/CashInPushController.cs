﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Http;
using System.Security.Cryptography.X509Certificates;

namespace M2020.Web.Api.Controllers._3rdPartyAPIs
{
    [RoutePrefix("api/cashinpush")]
    public class CashInPushController : ApiController
    {
        // POST: api/CashInPush
        [HttpPost]
        [Route("")]
        public void Post([FromBody]string webRequestBody)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
            HttpWebRequest request = WebRequest.Create("https://sandbox.api.visa.com/visadirect/mvisa/v1/cashinpushpayments") as HttpWebRequest;
            request.Method = "POST";

            // Add headers
            string userID = "6WSVVG4IRBPDP947BZJU21NpJOlfDZ_3pOLfGCN91_lwhHlrg";
            string password = "Z16icVADonsnmVsUX4N";
            string authString = userID + ":" + password;
            var authStringBytes = System.Text.Encoding.UTF8.GetBytes(authString);
            string authHeaderString = Convert.ToBase64String(authStringBytes);
            var postContent = @"{
                                ""acquirerCountryCode"": ""643"",
                                ""acquiringBin"": ""400171"",
                                ""amount"": ""124.05"",
                                ""businessApplicationId"": ""CI"",
                                ""cardAcceptor"": {
                                               ""address"": {
                                                   ""city"": ""Bangalore"",
                                ""country"": ""IN""
                                               },
                                ""idCode"": ""ID -Code123"",
                                ""name"": ""Test Merchant""
                                },
                                ""localTransactionDateTime"": ""2018-10-21T01:16:47"",
                                ""merchantCategoryCode"": ""6012"",
                                ""recipientPrimaryAccountNumber"": ""4123640062698797"",
                                ""retrievalReferenceNumber"": ""430000367618"",
                                ""senderAccountNumber"": ""4541237895236"",
                                ""senderName"": ""Mohammed Qasim"",
                                ""senderReference"": ""1234"",
                                ""systemsTraceAuditNumber"": ""313042"",
                                ""transactionCurrencyCode"": ""840""
                                }";

            //content.Headers.Add("Authorization", "Basic " + authHeaderString);
            var dataStream = request.GetRequestStream();
            byte[] bytes = Encoding.ASCII.GetBytes(postContent);
            dataStream.Write(bytes, 0, bytes.Length);

            // truning on tracing
            request.ContentType = "application/json";
            request.Accept = "application/json";
            request.Headers["Authorization"] = "Basic " + authHeaderString;
            // Add certificate
            var certificate = new X509Certificate2("C:\\sources\\ifta.p12", "ifta");

            request.ClientCertificates.Add(certificate);


            ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                ex.ToString();
            }
        }

        // PUT: api/CashInPush/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/CashInPush/5
        public void Delete(int id)
        {
        }
    }
}
