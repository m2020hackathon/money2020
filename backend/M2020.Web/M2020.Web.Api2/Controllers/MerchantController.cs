﻿using M2020.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace M2020.Web.Api.Controllers
{
    [RoutePrefix("api/merchant")]
    public class MerchantController : ApiController
    {
        [HttpGet]
        [Route("consumer")]
        public MerchantConsumer GetMerchantConsumer()
        {
            return null;
        }
    }
}
