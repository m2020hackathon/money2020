﻿using M2020.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace M2020.Web.Api.Controllers.Cognitive
{
    [RoutePrefix("api/cognitive/persongroup")]
    public class PersonGroupController : ApiController
    {
        [HttpGet]
        [Route("")]
        public string GetGroups()
        {
            var client = new HttpClient();

            var settings = new Settings();
            var uri = new Uri(settings.Url + "persongroups");
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "7134b091e1bd4f1f81af0f79c8012932");
            var result = client.GetAsync(uri).Result;
            return result.Content.ReadAsStringAsync().Result;
        }
        [HttpGet]
        [Route("{groupid}")]
        public string GetGroup(string groupid)
        {
            var client = new HttpClient();

            var settings = new Settings();
            var uri = new Uri(settings.Url + "persongroups/"+groupid);
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "7134b091e1bd4f1f81af0f79c8012932");
            var result = client.GetAsync(uri).Result;
            return result.Content.ReadAsStringAsync().Result;
        }
        [HttpGet]
        [Route("{groupid}/persons")]
        public string GetGroupPersons(string groupid)
        {
            var client = new HttpClient();

            var settings = new Settings();
            var uri = new Uri(settings.Url + "persongroups/" + groupid+"/persons");
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "7134b091e1bd4f1f81af0f79c8012932");
            var result = client.GetAsync(uri).Result;
            return result.Content.ReadAsStringAsync().Result;
        }
    }
}
