﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web.Http;

namespace M2020.Web.Api.Controllers._3rdPartyAPIs
{
    [RoutePrefix("api/iddocument")]
    public class IdDocumentController : ApiController
    {
       
        // POST: api/IdDocument
        [HttpPost]
        [Route("")]
        public void Post([FromBody]string value)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
            HttpWebRequest request = WebRequest.Create("https://sandbox.api.visa.com/identitydocuments/v1/documentauthentication") as HttpWebRequest;
            request.Method = "POST";

            // Add headers
            string userID = "IETYDG7957H7FEZ7CW2T21C7WIt4aCWv1w4IWZOsYNg_hO4HM";
            string password = "zdBo9GQ4NgIn1BfHDQcnd2I";
            string authString = userID + ":" + password;
            var authStringBytes = System.Text.Encoding.UTF8.GetBytes(authString);
            string authHeaderString = Convert.ToBase64String(authStringBytes);
            var postContent = @"{
                                --VISA_MESSAGE_BOUNDARY
                                Content-Disposition: form-data; name=""jsonMessage""
                                Content - Type: application / json; charset = UTF - 8
                                Content - Transfer - Encoding: 8bit
                                { ""messageTraceId"":""843810ae-d2d2-468d-a302-b516cadfd24a"",""documentCountryCode"":""US""}
                                            --VISA_MESSAGE_BOUNDARY
                                Content - Disposition: form - data; name = ""documentFront""; filename = ""driversLicense.jpg""
                                Content - Type: image / jpeg
                                Content - Transfer - Encoding: binary
                                      [Binary document front goes here]
                                    --VISA_MESSAGE_BOUNDARY
                                Content - Disposition: form - data; name = ""documentBack""; filename = ""driversLicenseBack.jpg""
                                 Content - Type: image / jpeg
                                Content - Transfer - Encoding: binary
                                       [Binary document back goes here]
                                --VISA_MESSAGE_BOUNDARY""
                            }";

            //content.Headers.Add("Authorization", "Basic " + authHeaderString);
            var dataStream = request.GetRequestStream();
            byte[] bytes = Encoding.ASCII.GetBytes(postContent);
            dataStream.Write(bytes, 0, bytes.Length);

            // truning on tracing
            request.ContentType = "Content-Type: multipart/form-data; boundary=VISA_MESSAGE_BOUNDARY";
            request.Accept = "application/json";
            request.Headers["Authorization"] = "Basic " + authHeaderString;
            // Add certificate
            var certificate = new X509Certificate2("C:\\sources\\ifta.p12", "ifta");

            request.ClientCertificates.Add(certificate);

            ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                ex.ToString();
            }
        }

        // PUT: api/IdDocument/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/IdDocument/5
        public void Delete(int id)
        {
        }
    }
}
