﻿using System.Web;
using System.Web.Mvc;

namespace M2020.Web.Api2
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
