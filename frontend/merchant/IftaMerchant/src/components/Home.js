// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

class Home extends Component {
    render() {
      return (
        <View style={styles.container}>
          <View className="MiniProfile"></View>
          <View className="TransactionList"></View>
        </View>
      );
    }
}
  
const styles = StyleSheet.create({
    
});

export default Home;
