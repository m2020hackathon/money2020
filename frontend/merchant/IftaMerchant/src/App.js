// import all dependencies and config required
import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

// import all components
import Login from './components/Login';
import Home from './components/Home';

class App extends Component {

  render() {
    return (
      <Router>
        <Scene key='root'>
          <Scene
            key='login'
            hideNavBar
            component={Login}
          />
          <Scene
            key='home'
            hideNavBar
            component={Home}
            initial
          />
        </Scene>
      </Router>
    );
  }
}

export default App;
