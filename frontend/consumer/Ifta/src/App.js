// import all dependencies and config required
import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

// import all components
import DummyPhoto from './components/DummyPhoto';
import Login from './components/Login';
import Home from './components/Home';
import Documents from './components/Documents';
import Levels from './components/Levels';
import LevelOffers from './components/LevelOffers';
import Confirmation from './components/Confirmation';
import DocumentUpload from './components/DocumentUpload';
import RecordTransactionAmount from './components/RecordTransactionAmount';
import RecordTransactionImage from './components/RecordTransactionImage';
import SendMoneyList from './components/SendMoneyList';
import SendMoneyAmount from './components/SendMoneyAmount';
import PayPalWebView from './components/PayPalWebView';

class App extends Component {

  render() {
    return (
      <Router>
        <Scene key='root'>
          <Scene
            key='dummyPhoto'
            hideNavBar
            component={DummyPhoto}
          />
          <Scene
            key='login'
            hideNavBar
            component={Login}
          />
          <Scene
            key='home'
            hideNavBar
            component={Home}
            initial
          />
          <Scene
            key='documents'
            hideNavBar
            component={Documents}
          />
          <Scene
            key='levels'
            hideNavBar
            component={Levels}
          />
          <Scene
            key='levelOffers'
            hideNavBar
            component={LevelOffers}
          />
          <Scene
            key='confirmation'
            hideNavBar
            component={Confirmation}
          />
          <Scene
            key='documentUpload'
            hideNavBar
            component={DocumentUpload}
          />
          <Scene
            key='recordTransactionImage'
            hideNavBar
            component={RecordTransactionImage}
          />
          <Scene
            key='recordTransactionAmount'
            hideNavBar
            component={RecordTransactionAmount}
          />
          <Scene
            key='sendMoneyList'
            hideNavBar
            component={SendMoneyList}
          />
          <Scene
            key='sendMoneyAmount'
            hideNavBar
            component={SendMoneyAmount}
          />
          <Scene
            key='payPalWebView'
            hideNavBar
            component={PayPalWebView}
          />
        </Scene>
      </Router>
    );
  }
}

export default App;
