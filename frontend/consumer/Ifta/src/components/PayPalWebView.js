import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    ScrollView,
    WebView
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

import MiniProfile from './common/MiniProfile';
import LevelOffer from './common/LevelOffer';
import TabBar from './common/TabBar'
import User from './common/User';

const { height, width } = Dimensions.get('window');

class PayPalWebView extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { Container} = styles;

        console.log(this.props);
        return (
            <View style={Container}>
                <WebView
                    source={{uri: this.props.href}}
                    style={{marginTop: 20, height: 0.9 * height}}
                />
                <View style={{ width: '100%', height: 0.1 * height, backgroundColor: 'white' }}>
                    <TouchableOpacity style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }} onPress={() => Actions.push('home')}>
                        <Text style={{ fontSize: 18 }}>Back</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    Container: {
        height: height,
        // alignItems: 'center',
        // backgroundColor: '#FAFAFA',
        // paddingTop: '10%',
        flex: 1
    }
});

export default PayPalWebView;
