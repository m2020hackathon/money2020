// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    ScrollView
  } from 'react-native';
import Modal from "react-native-modal";
import { Actions } from 'react-native-router-flux';

const { height, width } = Dimensions.get('window');

class Confirmation extends Component {

    constructor(props) {
        super(props);

        this.renderConfirmationTitle = this.renderConfirmationTitle.bind(this);
        this.renderConfirmationSubTitle = this.renderConfirmationSubTitle.bind(this);

        this.state = {
            modalVisible: true
        }
    }

    renderConfirmationTitle(successType) {
        if (successType == 'documentUploaded') {
            return 'Document Recieved!';
        } else if (successType == 'paymentRegistered') {
            return 'Payment Registered!';
        } else if (successType == 'paymentSent') {
            return 'Payment Sent!';
        }
    }

    renderConfirmationSubTitle(successType) {
        if (successType == 'documentUploaded') {
            return 'Keep going! Identification opens more doors for you';
        } else if (successType == 'paymentRegistered') {
            return 'Just earned 35 points! Great work.';
        } else if (successType == 'paymentSent') {
            return 'Virtual Cash is just like real cash! But virtual.';
        }
    }

    render() {
        const { Container, ModalImageView, ModalView, ModalDetails, ModalImage, ModalButton, ModalButtonText } = styles;

        return (
            <View style={Container}>
                <Modal isVisible={this.state.modalVisible}>
                    <View style={ModalView}>
                        <View style={ModalImageView}>
                            <Image source={require('../images/Check.png')} style={ModalImage} />
                        </View>
                        <View style={ModalDetails}>
                            <Text style={{ marginBottom: 20, color: '#9B68FF', fontSize: 18 }}>{this.renderConfirmationTitle(this.props.successType)}</Text>
                            <Text style={{ color: '#AAAFB5', fontSize: 14 }}>{this.renderConfirmationSubTitle(this.props.successType)}</Text>
                        </View>
                        <TouchableOpacity style={ModalButton} onPress={() => {
                            this.setState({modalVisible: false})
                            Actions.push('home')
                        }}>
                            <Text style={ModalButtonText}>COMPLETE</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center'
    },
    ModalImageView: {
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center'
    },
    ModalImage: {
        bottom: 50
    },
    ModalView: {
        height: 0.5 * height,
        width: 0.9 * width,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        borderRadius: 20,
        padding: '2%'
    },
    ModalDetails: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '20%',
        flex: 5
    },
    ModalButton: {
        flex: 2,
        width: '80%',
        backgroundColor: '#996CFF',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    ModalButtonText: {
        color: 'white',
        fontSize: 16
    }
});

export default Confirmation;
