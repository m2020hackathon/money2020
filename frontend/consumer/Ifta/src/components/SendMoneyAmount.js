// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    ScrollView,
    TextInput,
    Keyboard,
    WebView
  } from 'react-native';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';

import MiniProfile from './common/MiniProfile';
import Document from './common/Document';
import TabBar from './common/TabBar'

const { height, width } = Dimensions.get('window');

class SendMoneyAmount extends Component {

    constructor(props) {
        super(props);

        this.state = {
            transactionAmount: '$ 0'
        }
    }

    render() {
        const { Container, Title, SubTitle, AmountInput, TransactionButtonsView, TransactionButton } = styles;

        return (
            <View style={Container}>
                <Text style={Title}>Enter Transaction Amount</Text>
                <Text style={SubTitle}>Total value of goods</Text>
                <TextInput
                    style={AmountInput}
                    onChangeText={(text) => this.setState({transactionAmount: text})}
                    value={this.state.transactionAmount}
                    keyboardType = "numeric"
                    autoFocus
                    />
                <View style={TransactionButtonsView}>
                    <TouchableOpacity onPress={() => Actions.push('home')} style={[TransactionButton, {backgroundColor: '#F1F1F6'}]}>
                        <Text>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        // Request for PayPal Payment
                        axios.post('https://m2020webapi.azurewebsites.net/api/person/sendmoney', {
                            amount: {
                              value: Number(this.state.transactionAmount.substr(2)),
                              currency: "USD"
                            },
                            payee: {
                              id: '+1' + this.props.phoneNumber,
                              type: "PHONE"
                            },
                            payment_type: "PERSONAL"
                        })
                        .then(data => {
                            console.log(data)
                            console.log(Number(this.state.transactionAmount.substr(2)))
                            Actions.push('payPalWebView', {href: data.data[0].href})
                        })
                        //Actions.push('confirmation', {successType: 'paymentSent'})
                    }} style={[TransactionButton, {backgroundColor: '#488BE7'}]}>
                        <Text style={{ color: 'white' }}>Confirm</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    Container: {
        height: height,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#FAFAFA',
        paddingTop: '10%'
    },
    Title: {
        fontSize: 20,
        color: '#36A0EA',
        textAlign: 'left',
        width: '90%',
        marginBottom: 10
    },
    SubTitle: {
        fontSize: 16,
        color: '#AAAFB5',
        textAlign: 'left',
        width: '90%',
        marginBottom: 20
    },
    AmountInput: {
        height: 40,
        width: '90%', 
        backgroundColor: '#F1F1F6',
        borderRadius: 15,
        marginBottom: 20,
        textAlign: 'center'
    },
    TransactionButtonsView: {
        width: '90%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    TransactionButton: {
        width: '48%',
        height: 40,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default SendMoneyAmount;
