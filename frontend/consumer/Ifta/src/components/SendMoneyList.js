// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    ScrollView
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

import MiniProfile from './common/MiniProfile';
import LevelOffer from './common/LevelOffer';
import TabBar from './common/TabBar'
import User from './common/User';

const { height, width } = Dimensions.get('window');

class SendMoneyList extends Component {

    constructor(props) {
        super(props);

        this.renderUserList = this.renderUserList.bind(this);
    }

    renderUserList(users) {
        return users.map((user, index) => {
            return <User key={index} user={user} />
        })
    }

    render() {
        const { Container, UserList, SectionHeading, Title, SubTitle } = styles;

        return (
            <View style={Container}>
                <ScrollView style={UserList} contentContainerStyle={{ alignItems: 'center'}}>
                    <Text style={SectionHeading}>Send Digital Money</Text>
                    <Text style={Title}>Enter Transaction Amount</Text>
                    <Text style={SubTitle}>Total value of goods</Text>
                    {this.renderUserList([
                        {
                            userName: 'Amit Belal',
                            phone: '6172498243'
                        },
                        {
                            userName: 'Randy Ortega',
                            phone: '2982438291'
                        },
                        {
                            userName: 'Will Gold',
                            phone: '2099381002'
                        }
                    ])}
                </ScrollView>
                <TabBar />
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    Container: {
        height: height,
        alignItems: 'center',
        backgroundColor: '#FAFAFA',
        paddingTop: '10%'
    },
    Title: {
        fontSize: 20,
        color: '#36A0EA',
        textAlign: 'left',
        width: '90%',
        marginBottom: 10
    },
    SubTitle: {
        fontSize: 16,
        color: '#AAAFB5',
        textAlign: 'left',
        width: '90%',
        marginBottom: 20
    },
    UserList: {
        minHeight: 0.8 * height,
        height: 'auto',
        width: width,
        backgroundColor: 'white',
        overflow: 'scroll'
    },
    SectionHeading: {
        color: '#36A0EA',
        fontSize: 24,
        marginTop: 15,
        marginBottom: 15
    }
});

export default SendMoneyList;
