// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    ScrollView
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

import MiniProfile from './common/MiniProfile';
import Level from './common/Level';
import TabBar from './common/TabBar'

const { height, width } = Dimensions.get('window');

class Levels extends Component {

    constructor(props) {
        super(props);

        this.renderLevels = this.renderLevels.bind(this);
    }

    renderLevels(levels) {
        return levels.map((level, index) => {
            return <Level key={index} level={level} />
        })
    }

    render() {
        const { Container, LevelList, SectionHeading } = styles;

        return (
            <View style={Container}>
                <MiniProfile height={0.2} containerHeight={height} containerWidth={width} viewProfileInfo={false} />
                <ScrollView style={LevelList} contentContainerStyle={{ alignItems: 'center'}}>
                    <Text style={SectionHeading}>Levels</Text>
                    {this.renderLevels([
                        {
                            name: 'Bronze',
                            color: '#C29800',
                            isUnlocked: true
                        },
                        {
                            name: 'Silver',
                            color: '#959595',
                            isUnlocked: true
                        },
                        {
                            name: 'Gold',
                            color: '#F76B1C',
                            isUnlocked: false
                        }
                    ])}
                </ScrollView>
                <TabBar />
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    Container: {
        height: height,
        alignItems: 'center',
        backgroundColor: '#FAFAFA'
    },
    LevelList: {
        minHeight: 0.8 * height,
        width: width,
        backgroundColor: 'white'
    },
    SectionHeading: {
        color: '#36A0EA',
        fontSize: 24,
        marginTop: 15,
        marginBottom: 15
    }
});

export default Levels;
