// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    ScrollView
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

import MiniProfile from './common/MiniProfile';
import LevelOffer from './common/LevelOffer';
import TabBar from './common/TabBar'

const { height, width } = Dimensions.get('window');

class LevelOffers extends Component {

    constructor(props) {
        super(props);

        this.renderLevelOffers = this.renderLevelOffers.bind(this);
    }

    renderLevelOffers(levelOffers) {
        return levelOffers.map((levelOffer, index) => {
            return <LevelOffer key={index} />
        })
    }

    render() {
        const { Container, LevelOfferList, SectionHeading } = styles;

        return (
            <View style={Container}>
                <ScrollView style={LevelOfferList} contentContainerStyle={{ alignItems: 'center'}}>
                    <Text style={SectionHeading}>Offers</Text>
                    {this.renderLevelOffers([1,2,3])}
                </ScrollView>
                <TabBar />
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    Container: {
        height: height,
        alignItems: 'center',
        backgroundColor: '#FAFAFA'
    },
    LevelOfferList: {
        minHeight: 0.8 * height,
        height: 'auto',
        width: width,
        backgroundColor: 'white',
        overflow: 'scroll'
    },
    SectionHeading: {
        color: '#36A0EA',
        fontSize: 24,
        marginTop: 15,
        marginBottom: 15
    }
});

export default LevelOffers;
