// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    Picker,
    Alert,
    Image
  } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { RNCamera, FaceDetector } from 'react-native-camera';
import axios from 'axios';

class RecordTransactionImage extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }
    render() {

        const {Container, Preview, CaptureButtonView, CaptureButton, CameraFooter} = styles;
      return (
        <View style={Container}>
          <RNCamera
              ref={ref => {
                this.camera = ref;
              }}
              style = {Preview}
              type={RNCamera.Constants.Type.back}
              flashMode={RNCamera.Constants.FlashMode.off}
              permissionDialogTitle={'Permission to use camera'}
              permissionDialogMessage={'We need your permission to use your camera phone'}
              onGoogleVisionBarcodesDetected={({ barcodes }) => {
                console.log(barcodes)
              }}
          />
          <View style={CameraFooter}>
            <TouchableOpacity
                onPress={this.takePicture.bind(this)}
                style = {CaptureButtonView}
            >
                <Image source={require('../images/Camera.png')} style={CaptureButton} />
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  
    takePicture = async function() {
      if (this.camera) {
        const options = { quality: 0.5, base64: true };
        const data = await this.camera.takePictureAsync(options);

        axios.post('https://m2020webapi.azurewebsites.net/api/photo/identify', {
            imageData: data.base64
        })
        .then(data => {
            Actions.push('recordTransactionAmount', {person: data.data})
        })
        .catch(error => Alert.alert(error));
      }
    };
}
  
const styles = StyleSheet.create({
    Container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: 'black'
    },
    Preview: {
      flex: 9,
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    CameraFooter: {
        flex: 1,
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    CaptureButtonView: {
        backgroundColor: 'black',
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 100,
        height: '120%',
        width: '20%',
        marginBottom: 20,
        borderWidth: 10,
        borderColor: '#FAFAFA',
        backgroundColor: '#FAFAFA'
    },
    CaptureButton: {
        flex: 1,
        width: 70,
        resizeMode: 'contain'
    }
});

export default RecordTransactionImage;
