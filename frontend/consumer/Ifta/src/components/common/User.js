// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

class User extends Component {
    render() {

        const {TransactionView, CustomerPictureView, CustomerPicture, TransactionInfoView, CustomerName, TransactionDate, TransactionAmountView, TransactionAmount, TransactionAmountText} = styles;

        return (
            <View style={TransactionView}>
                <View style={CustomerPictureView}>
                    <View style={CustomerPicture}></View>
                </View>
                <View style={TransactionInfoView}>
                    <Text style={CustomerName}>{this.props.user.userName}</Text>
                    <Text style={TransactionDate}>{this.props.user.phone}</Text>
                </View>
                <View style={TransactionAmountView}>
                    <TouchableOpacity style={TransactionAmount} onPress={() => Actions.push('sendMoneyAmount', {phoneNumber: this.props.user.phone})}>
                        <Text style={TransactionAmountText}>Send</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    TransactionView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: 70,
        borderBottomWidth: 1,
        borderBottomColor: '#FAFAFA'
    },
    CustomerPictureView: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    CustomerPicture: {
        width: 40,
        height: 40,
        backgroundColor: 'black',
        borderRadius: 30
    },
    TransactionInfoView: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'space-around',
        flex: 5,
        height: '55%'
    },
    CustomerName: {
        fontSize: 14
    },
    TransactionDate: {
        fontSize: 11,
        color: '#7476A5'
    },
    TransactionAmountView: {
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center'
    },
    TransactionAmount: {
        backgroundColor: "#03DFF4",
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 20
    },
    TransactionAmountText: {
        color: 'white'
    }
});

export default User;
