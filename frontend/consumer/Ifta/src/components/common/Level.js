// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    Alert
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

class Level extends Component {
    render() {

        const {LevelView, LevelNameView, LevelName, LevelLockView, LevelLock} = styles;

        return (
            <TouchableOpacity style={[LevelView, {backgroundColor: this.props.level.color}]} onPress={() => {
                if (this.props.level.isUnlocked) {
                    Actions.push('levelOffers')
                } else {
                    Alert.alert('This level is locked!')
                }
            }}>
                <View style={LevelNameView}>
                    <Text style={LevelName}>{this.props.level.name}</Text>
                </View>
                <View style={LevelLockView}>
                    {this.props.level.isUnlocked ? <Image source={require('../../images/UnlockedLevel.png')} style={LevelLock} /> : <Image source={require('../../images/LockedLevel.png')} style={LevelLock} />}
                </View>
            </TouchableOpacity>
        );
    }
}
  
const styles = StyleSheet.create({
    LevelView: {
        width: '80%',
        height: '40%',
        borderRadius: 20,
        marginTop: 10,
        marginBottom: 10,
        flexDirection: 'row'
    },
    LevelNameView: {
        flex: 7,
        alignItems: 'center',
        justifyContent: 'center'
    },
    LevelName: {
        fontSize: 30,
        color: 'white'
    },
    LevelLockView: {
        flex: 3,
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'center',
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20
    },
    LevelLock: {
        width: 50,
        height: 50
    }
});

export default Level;
