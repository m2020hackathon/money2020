// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

class Document extends Component {
    render() {

        const {DocumentView, DocumentAction, DocumentDetailView, DocumentName, DocumentUploadDate, DocumentDetails} = styles;

        return (
            <View style={DocumentView}>
                <TouchableOpacity style={DocumentAction} onPress={() => {
                    if (this.props.document.action == 'Upload') {
                        Actions.push('documentUpload');
                    }
                }}>
                    <Text>{this.props.document.action}</Text>
                </TouchableOpacity>
                <View style={DocumentDetailView}>
                    <Text style={DocumentName}>{this.props.document.documentName}</Text>
                    {this.props.document.documentDate ? <Text style={DocumentUploadDate}>{this.props.document.documentDate}</Text> : null}
                    {this.props.document.documentDetails ? <Text style={DocumentDetails}>{this.props.document.documentDetails}</Text> : null}
                </View>
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    DocumentView: {
        width: '80%',
        height: '40%',
        borderRadius: 20,
        marginTop: 10,
        marginBottom: 10,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#FAFAFA'
    },
    DocumentAction: {
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FAFAFA'
    },
    DocumentDetailView: {
        flex: 7,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'space-around',
        paddingLeft: '5%',
        paddingRight: '5%',
        backgroundColor: '#173143',
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20
    },
    DocumentName: {
        fontSize: 14,
        color: 'white'
    }, 
    DocumentUploadDate: {
        fontSize: 11,
        color: 'white'
    },
    DocumentDetails: {
        fontSize: 9,
        color: 'white'
    }
});

export default Document;
