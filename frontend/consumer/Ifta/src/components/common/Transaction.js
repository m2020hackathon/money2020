// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image
  } from 'react-native';
import { Actions } from 'react-native-router-flux';
// import Moment from 'react-moment';

class Transaction extends Component {

    constructor(props) {
        super(props);

        this.renderPayeeName = this.renderPayeeName.bind(this);
        this.renderPayeePicture = this.renderPayeePicture.bind(this);
    }

    renderPayeeName(transaction) {
        if (transaction.FromPersonId == 1) {
            return 'Gabe Ficht';
        } else if (transaction.FromPersonId == 2) {
            return 'Waseem Shabout';
        } else if (transaction.FromPersonId == 3) {
            return 'Jianguo Jiang';
        } else if (transaction.FromPersonId == 4) {
            return 'Gabe Ficht';
        } else if (transaction.FromPersonId == 5) {
            return 'Kush Desai';
        } else if (transaction.FromPersonId == 6) {
            return 'Arpit Bansal';
        }
    }

    renderPayeePicture(transaction) {
        if (transaction.FromPersonId == 1) {
            return 'https://bloximages.chicago2.vip.townnews.com/newburyportnews.com/content/tncms/assets/v3/editorial/b/c7/bc7b780c-a9f9-5a5d-92e0-232e139e72be/53e7cf4734860.image.jpg';
        } else if (transaction.FromPersonId == 2) {
            return 'https://cdn-images-1.medium.com/max/1600/1*BKX61qcRPU1W6aq_ERIVnQ.png';
        } else if (transaction.FromPersonId == 3) {
            return 'https://i.imgur.com/4mx7viQ.jpg';
        } else if (transaction.FromPersonId == 4) {
            return 'https://bloximages.chicago2.vip.townnews.com/newburyportnews.com/content/tncms/assets/v3/editorial/b/c7/bc7b780c-a9f9-5a5d-92e0-232e139e72be/53e7cf4734860.image.jpg';
        } else if (transaction.FromPersonId == 5) {
            return 'https://i.imgur.com/MvKVUhk.jpg';
        } else if (transaction.FromPersonId == 6) {
            return 'https://res-3.cloudinary.com/crunchbase-production/image/upload/c_thumb,h_256,w_256,f_auto,g_faces,z_0.7,q_auto:eco/v1481432936/plpldhyiv1winlpmaufq.jpg';
        }
    }

    render() {

        const {TransactionView, CustomerPictureView, CustomerPicture, TransactionInfoView, CustomerName, TransactionDate, TransactionAmountView, TransactionAmount, TransactionAmountText} = styles;

        return (
            <View style={TransactionView}>
                <View style={CustomerPictureView}>
                    <View style={CustomerPicture}>
                        <Image style={{
                            width: '100%', 
                            height: '100%', 
                            borderRadius: 20
                        }} source={{ uri: this.renderPayeePicture(this.props.transaction) }} />
                    </View>
                </View>
                <View style={TransactionInfoView}>
                    <Text style={CustomerName}>{this.renderPayeeName(this.props.transaction)}</Text>
                    <Text style={TransactionDate}>{'October ' + Math.floor(Math.random() * 30 + 1) + ', 2018'}</Text>
                </View>
                <View style={TransactionAmountView}>
                    <View style={TransactionAmount}>
                        <Text style={TransactionAmountText}>{'$' + " " + this.props.transaction.Amount}</Text>
                    </View>
                </View>
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    TransactionView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: 70,
        borderBottomWidth: 1,
        borderBottomColor: '#FAFAFA'
    },
    CustomerPictureView: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    CustomerPicture: {
        width: 40,
        height: 40,
        backgroundColor: 'black',
        borderRadius: 30
    },
    TransactionInfoView: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'space-around',
        flex: 5,
        height: '55%'
    },
    CustomerName: {
        fontSize: 14
    },
    TransactionDate: {
        fontSize: 11,
        color: '#7476A5'
    },
    TransactionAmountView: {
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center'
    },
    TransactionAmount: {
        backgroundColor: "#03DFF4",
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 20
    },
    TransactionAmountText: {
        color: 'white'
    }
});

export default Transaction;
