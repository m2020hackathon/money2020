// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

class MiniProfile extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { height, containerHeight, containerWidth } = this.props;

        const { MiniProfile, ProfilePictureView, ProfilePicture, ProfileName, ProfileAccount, ProfileStats, ProfileStat, ProfileStatHeading, ProfileStatContent, StatImage } = styles;
        
        return (
            <View style={[MiniProfile, {height: height * containerHeight, width: containerWidth}]}>
                <View style={ProfilePictureView}>
                    <View style={ProfilePicture}>
                        <Image style={{ width: '100%', height: '100%', borderRadius: 35}} source={{uri: 'https://pbs.twimg.com/media/DMrLh4KVwAAw4GG.jpg'}} />
                    </View>
                    {this.props.viewProfileInfo ? (
                        // <Text style={ProfileName}>{this.props.person.FirstName + " " + this.props.person.LastName}</Text>
                        <Text style={ProfileName}>Loren Koss</Text>
                    ) : (
                        null
                    )}
                    {this.props.viewProfileInfo ? (
                        <Text style={ProfileAccount}>Act #1211312</Text>
                    ) : (
                        null
                    )}
                </View>
                <View style={ProfileStats}>
                    <View style={ProfileStat}>
                        <Image source={require('../../images/UserCheck.png')} style={StatImage}/>
                        <Text style={ProfileStatHeading}>Transactions</Text>
                        <Text style={ProfileStatContent}>{this.props.transactions}</Text>
                    </View>
                    <View style={ProfileStat}>
                        <Image source={require('../../images/Trophy.png')} style={StatImage}/>
                        <Text style={ProfileStatHeading}>Level</Text>
                        <Text style={ProfileStatContent}>Silver</Text>
                    </View>
                </View>
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    MiniProfile: {
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'row'
    },
    ProfilePictureView: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        flex: 4,
        height: '70%'
    },
    ProfilePicture: {
        height: 80,
        width: 80,
        backgroundColor: 'black',
        borderRadius: 50,
        borderWidth: 4,
        borderColor: '#00D3B8'
    },
    ProfileName: {
        fontSize: 16
    },
    ProfileAccount: {
        fontSize: 11,
        color: '#7476A5'
    },
    ProfileStats: {
        alignItems: 'center',
        justifyContent: 'space-around',
        flex: 6,
        flexDirection: 'row',
        height: '40%'
    },
    ProfileStat: {
        alignItems: 'center',
        justifyContent: 'space-around',
        height: '100%'
    },
    ProfileStatHeading: {
        fontSize: 14
    },
    ProfileStatContent: {
        fontSize: 14,
        color: '#7476A5'
    },
    StatImage: {
        width: 20,
        height: 20
    }
});

export default MiniProfile;