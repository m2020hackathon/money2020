// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

class LevelOffer extends Component {
    render() {

        const {LevelOfferView, LevelOfferImageView, LevelOfferImage, LevelOfferDetailView, LevelOfferName, LevelOfferDetail, LevelOfferApplyButton, LevelOfferApplyButtonText} = styles;

        return (
            <View style={LevelOfferView}>
                <View style={LevelOfferImageView}>
                    <Image source={require('../../images/Xoom.png')} style={LevelOfferImage} />
                </View>
                <View style={LevelOfferDetailView}>
                    <Text style={LevelOfferName}>Xoom</Text>
                    <Text style={LevelOfferDetail}>Xoom is a digital wallet that allows you to send and recieve money from anywhere in the world. You can use this to send money to family members, or recieve payment from employers!</Text>
                </View>
                <TouchableOpacity style={LevelOfferApplyButton}>
                    <Text style={LevelOfferApplyButtonText}>Apply</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    LevelOfferView: {
        width: '80%',
        height: '70%',
        marginTop: 10,
        marginBottom: 10,
        flexDirection: 'column',
        borderWidth: 1,
        borderColor: '#FAFAFA'
    },
    LevelOfferImageView: {
        flex: 6,
        backgroundColor: '#173143',
        width: '100%',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    LevelOfferImage: {
        height: '50%',
        width: '80%',
        resizeMode: 'center'
    },
    LevelOfferDetailView: {
        flex: 3,
        width: '100%',
        alignItems: 'flex-start',
        justifyContent: 'space-around',
        padding: '5%',
        backgroundColor: '#FAFAFA'
    },
    LevelOfferApplyButton: {
        flex: 1,
        width: '100%',
        backgroundColor: '#00D3B8',
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20
    },
    LevelOfferApplyButtonText: {
        color: 'white'
    }
});

export default LevelOffer;
