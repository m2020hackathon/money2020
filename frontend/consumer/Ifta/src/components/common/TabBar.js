// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

const { height, width } = Dimensions.get('window');

class TabBar extends Component {
    render() {

      const { TabBar, TabBarButton, TabBarIcon } = styles;

      return (
        <View style={TabBar}>
          <TouchableOpacity style={TabBarButton} onPress={() => Actions.push('home')}>
              <Image source={require('../../images/House.png')} style={TabBarIcon} />
          </TouchableOpacity>
          <TouchableOpacity style={TabBarButton} onPress={() => Actions.push('documents')}>
              <Image source={require('../../images/Sheet.png')} style={{ 
                  flex: 1,
                  width: 22,
                  resizeMode: 'contain' }} 
              />
          </TouchableOpacity>
          <TouchableOpacity style={{
              borderRadius: 100,
              height: '160%',
              borderWidth: 10,
              borderColor: '#FAFAFA',
              backgroundColor: '#FAFAFA'
          }} onPress={() => Actions.push('recordTransactionImage')}>
              <Image source={require('../../images/Plus.png')} style={{ 
                  flex: 1,
                  width: 70,
                  resizeMode: 'contain',
                  marginBottom: 20
                }} 
              />
          </TouchableOpacity>
          <TouchableOpacity style={TabBarButton} onPress={() => Actions.push('levels')}>
              <Image source={require('../../images/GreyTrophy.png')} style={TabBarIcon} />
          </TouchableOpacity>
          <TouchableOpacity style={TabBarButton} onPress={() => Actions.push('sendMoneyList')}>
              <Image source={require('../../images/GreyPerson.png')} style={TabBarIcon} />
          </TouchableOpacity>
      </View>
      );
    }
}
  
const styles = StyleSheet.create({
  TabBar: {
    position: 'absolute',
    bottom: 0,
    width: width,
    height: 0.1 * height,
    backgroundColor: '#FAFAFA',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    overflow: 'visible'
  },
  TabBarButton: {
      alignItems: 'center',
      justifyContent: 'center'
  },
  TabBarIcon: {
      flex: 1,
      width: 25,
      resizeMode: 'contain'
  }
});

export default TabBar;