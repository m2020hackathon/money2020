// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    ScrollView
  } from 'react-native';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';

import MiniProfile from './common/MiniProfile';
import Transaction from './common/Transaction';
import TabBar from './common/TabBar'

const { height, width } = Dimensions.get('window');

class Home extends Component {

    constructor(props) {
        super(props);

        this.renderTransactions = this.renderTransactions.bind(this);

        this.state = {
            userId: 1,
            transactions: []
        }
    }

    componentDidMount() {
        axios.get('https://m2020webapi.azurewebsites.net/api/transaction/user/' + this.state.userId)
        .then(data => {
            this.setState({ transactions: data.data })
        })
        .catch(error => console.log("Error"))
    }

    renderTransactions(transactions) {
        if (transactions.length > 0) {
            return transactions.map((transaction, index) => {
                return <Transaction key={index} transaction={transaction} />
            })
        } else {
            return <Text>No Transactions Found</Text>;
        }
    }

    render() {
        const { Container, TransactionList } = styles;

        return (
            <View style={Container}>
                {/* <MiniProfile height={0.3} containerHeight={height} containerWidth={width} viewProfileInfo={true} person={this.props.person} /> */}
                <MiniProfile height={0.3} containerHeight={height} containerWidth={width} viewProfileInfo={true} transactions={this.state.transactions.length} />
                <ScrollView style={TransactionList}>
                    {this.renderTransactions(this.state.transactions)}
                </ScrollView>
                <TabBar />
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    Container: {
        height: height,
        alignItems: 'center',
        backgroundColor: '#FAFAFA'
    },
    TransactionList: {
        minHeight: 0.7 * height,
        width: width,
        backgroundColor: 'white'
    }
});

export default Home;
