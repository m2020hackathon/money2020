// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    ScrollView
  } from 'react-native';
import { Actions } from 'react-native-router-flux';

import MiniProfile from './common/MiniProfile';
import Document from './common/Document';
import TabBar from './common/TabBar'

const { height, width } = Dimensions.get('window');

class Documents extends Component {

    constructor(props) {
        super(props);

        this.renderDocuments = this.renderDocuments.bind(this);
    }

    renderDocuments(documents) {
        return documents.map((document, index) => {
            return <Document key={index} document={document} />
        })
    }

    render() {
        const { Container, DocumentList, SectionHeading } = styles;

        return (
            <View style={Container}>
                <MiniProfile height={0.2} containerHeight={height} containerWidth={width} viewProfileInfo={false} />
                <ScrollView style={DocumentList} contentContainerStyle={{ alignItems: 'center'}}>
                    <Text style={SectionHeading}>Your Documents</Text>
                    {this.renderDocuments([
                        {
                            documentName: "Refugee ID",
                            documentDate: 'October 1, 2018',
                            documentDetails: 'UNHRC Issued ID',
                            action: 'View'
                        },
                        {
                            documentName: 'Passport',
                            documentDate: '',
                            documentDetails: 'National Identification',
                            action: 'Upload'
                        }
                    ])}
                </ScrollView>
                <TabBar />
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    Container: {
        height: height,
        alignItems: 'center',
        backgroundColor: '#FAFAFA'
    },
    DocumentList: {
        minHeight: 0.8 * height,
        width: width,
        backgroundColor: 'white'
    },
    SectionHeading: {
        color: '#36A0EA',
        fontSize: 24,
        marginTop: 15,
        marginBottom: 15
    }
});

export default Documents;
