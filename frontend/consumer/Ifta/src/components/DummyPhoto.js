// import all dependencies and config required
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    Picker,
    Alert
  } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { RNCamera, FaceDetector } from 'react-native-camera';
import axios from 'axios';

class DummyPhoto extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      action: 'Add'
    }
  }
    render() {
      return (
        <View style={styles.container}>
          <RNCamera
              ref={ref => {
                this.camera = ref;
              }}
              style = {styles.preview}
              type={RNCamera.Constants.Type.back}
              flashMode={RNCamera.Constants.FlashMode.off}
              permissionDialogTitle={'Permission to use camera'}
              permissionDialogMessage={'We need your permission to use your camera phone'}
              onGoogleVisionBarcodesDetected={({ barcodes }) => {
                console.log(barcodes)
              }}
          />
          <View style={{flex: 0, flexDirection: 'column', alignItems: 'center', justifyContent: 'space-around',}}>
            <TextInput
              style={{height: 40, width: '100%', borderColor: 'gray', borderWidth: 1, color: 'white'}}
              onChangeText={(text) => this.setState({action: text})}
              value={this.state.action}
            />
            <TextInput
              style={{height: 40, width: '100%', borderColor: 'gray', borderWidth: 1, color: 'white'}}
              onChangeText={(text) => this.setState({userId: text})}
              value={this.state.userId}
            />
            <TouchableOpacity
                onPress={this.takePicture.bind(this)}
                style = {styles.capture}
            >
                <Text style={{fontSize: 14}}> SNAP </Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  
    takePicture = async function() {
      if (this.camera) {
        const options = { quality: 0.5, base64: true };
        const data = await this.camera.takePictureAsync(options);
        if (this.state.action == 'Add') {
          axios.post('https://m2020webapi.azurewebsites.net/api/photo', {
            userId: this.state.userId,
            imageData: data.base64
          }).then(data => console.log(data).catch(error => console.log(error)));
        } else if (this.state.action == 'Identify') {
          axios.post('https://m2020webapi.azurewebsites.net/api/photo/identify', {
            imageData: data.base64
          }).then(data => {
            Alert.alert(data.data)
          }).catch(error => Alert.alert(error));
        } else if (this.state.action == 'Document') {
          axios.post('https://m2020webapi.azurewebsites.net/api/idDocument', {
            testData: ''
          }).then(data => {
            Actions.push('home', {person: data.data})
          }).catch(error => Alert.alert(error));
        }
      }
    };
}
  
const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: 'black'
    },
    preview: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    capture: {
      flex: 0,
      backgroundColor: '#fff',
      borderRadius: 5,
      padding: 15,
      paddingHorizontal: 20,
      alignSelf: 'center',
      margin: 20
    }
});

export default DummyPhoto;
